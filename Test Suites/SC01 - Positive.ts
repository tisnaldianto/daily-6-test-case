<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SC01 - Positive</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>2a69e01c-602d-4523-91b3-9f41b68ad78c</testSuiteGuid>
   <testCaseLink>
      <guid>dda3049d-8a0d-4a05-9156-4bc759d533ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Positive/S01/TC01</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3bf31b83-4e05-46d4-b619-c92140875122</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>3bf31b83-4e05-46d4-b619-c92140875122</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username1</value>
         <variableId>42f0aa1c-0151-48fb-bf28-86cf4cd051a6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>3bf31b83-4e05-46d4-b619-c92140875122</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>da4bc861-81db-4fa6-8662-d2748c02952d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2732e217-3794-43dd-9d1d-f9ebfd28b4b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Positive/S01/TC02</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8b3b8214-e4e5-46c6-a5b0-8eb183fe5da3</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>8b3b8214-e4e5-46c6-a5b0-8eb183fe5da3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username2</value>
         <variableId>2e4006ee-23c7-46f9-8373-a341211b6d96</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8b3b8214-e4e5-46c6-a5b0-8eb183fe5da3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>4aef4fa6-2dc3-4aff-ac55-a4a0c0ad6e43</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>836a6b9d-dcfd-46de-9103-cf64e892f10c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Positive/S01/TC03</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>bfafe603-0cc4-40b7-82e6-71e4b0173b31</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>bfafe603-0cc4-40b7-82e6-71e4b0173b31</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username3</value>
         <variableId>334b9a40-a217-491c-b4cf-c645fc6b2a40</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>bfafe603-0cc4-40b7-82e6-71e4b0173b31</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>fa005c2d-09f9-4887-9d10-49741ef4a633</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f148f4a2-cc71-4e41-a1f5-fc83a58ee748</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Positive/S01/TC04</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0e6646dd-d7cb-4499-8708-5638873d00ab</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>0e6646dd-d7cb-4499-8708-5638873d00ab</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username4</value>
         <variableId>4d00afdf-6b2d-4a9b-9ab7-1b7cd3f64d97</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0e6646dd-d7cb-4499-8708-5638873d00ab</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>39de3ceb-24e2-47cd-9b84-7660c94a39e0</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
