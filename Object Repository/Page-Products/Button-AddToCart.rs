<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button-AddToCart</name>
   <tag></tag>
   <elementGuidId>1ca33a84-b17e-46b1-875a-009f31014f49</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#add-to-cart-sauce-labs-backpack</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='add-to-cart-sauce-labs-backpack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9582f0de-fc03-45bf-9767-450ec240fc3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn_primary btn_small btn_inventory</value>
      <webElementGuid>78832948-dd0d-4c47-93e5-aa6c194ca720</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-backpack</value>
      <webElementGuid>d818822e-195d-4f93-949f-c1cd6ee2ee91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-backpack</value>
      <webElementGuid>e8456495-041d-40cc-8a9f-d0b4d80e94c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-backpack</value>
      <webElementGuid>513e73fc-dca3-4c55-bbe2-647884579761</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add to cart</value>
      <webElementGuid>4169910b-95ee-4cb5-a02c-39724b1d890b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;add-to-cart-sauce-labs-backpack&quot;)</value>
      <webElementGuid>c8fec796-3a83-4c0d-bd55-e96c79d15307</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='add-to-cart-sauce-labs-backpack']</value>
      <webElementGuid>fd91f723-f88a-42ab-bb5a-a66a05bc62c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inventory_container']/div/div/div[2]/div[2]/button</value>
      <webElementGuid>df9ebd5d-668e-42c3-9abc-ca38cc32541c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$29.99'])[1]/following::button[1]</value>
      <webElementGuid>6326320c-99ff-43c2-8f72-6437e95632af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sauce Labs Backpack'])[1]/following::button[1]</value>
      <webElementGuid>643ae9e3-f7ee-47f4-8d2f-8cb460f47f79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sauce Labs Bike Light'])[1]/preceding::button[1]</value>
      <webElementGuid>2647a29e-bb8f-4585-9165-80058deee57c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$9.99'])[1]/preceding::button[1]</value>
      <webElementGuid>d2dd0ca3-9262-49d0-99bb-3aa97bde687d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add to cart']/parent::*</value>
      <webElementGuid>b3ac8b53-0feb-4ab3-bd5f-e9b2f7756315</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>4d849f86-c0d6-4c41-80d9-5d1c3bf5fbe7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'add-to-cart-sauce-labs-backpack' and @name = 'add-to-cart-sauce-labs-backpack' and (text() = 'Add to cart' or . = 'Add to cart')]</value>
      <webElementGuid>a6a1ab96-1689-4eb2-9f8c-d10f9475f4f7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
